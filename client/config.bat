@echo off

rem Path to puppet
set _PUPPET_PATH=Puppet Labs\Puppet\bin\
if defined ProgramFiles(x86) set PUPPET_PATH="%ProgramFiles(x86)%\%_PUPPET_PATH%"
if not defined ProgramFiles(x86) set PUPPET_PATH="%ProgramFiles%\%_PUPPET_PATH%"

rem Path to git
set GIT_PATH=c:\git\
