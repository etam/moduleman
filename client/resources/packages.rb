#!/usr/bin/env ruby

# Copied from FileUtils.mkdir_p
def mkdir_p(path)
  path.sub!(%</\z>, '') # remove trailing '/'
  begin
    # optimize for the most common case
    Dir.mkdir path
  rescue SystemCallError
    stack = []
    until path == stack.last   # dirname("/")=="/", dirname("C:/")=="C:/"
      stack.push path
      path = File.dirname(path)
    end
    stack.reverse_each do |dir|
      begin
        Dir.mkdir dir
      rescue SystemCallError
        raise unless File.directory?(dir)
      end
    end
  end
end


# somehow on windows input later becomes empty
input = STDIN.read

require 'puppet'
require 'puppet/face'

Puppet.initialize_settings

$stderr.puts "info: downloading plugins"
mkdir_p(Puppet[:plugindest])
Puppet::Face[:plugin, '0'].download

$stderr.puts "info: listing packages"
current_state = Puppet::Face[:resource, '0'].search("package") \
  .reject { |resource| resource[:ensure].to_s == "absent" }
if Puppet::Util::Platform.windows?
  current_state.reject! { |resource| resource[:provider] != :chocolatey }
end


case ARGV[0]
when 'init'
  puts PSON.pretty_generate(current_state)

when 'update'
  orig_state = PSON.parse(input).map do |pson|
    resource = Puppet::Resource.from_pson(pson)
    # convert appropriate parameter values from string to symbols
    [:loglevel, :provider, :configfiles].each { |param| resource[param] = resource[param].to_sym }
    resource
  end
  
  # (current_state - orig_state).each ... does not work here.
  # Array's substraction operator removes identical objects, while include? uses object's == operator.
  current_state.each do |resource|
    #if resource[:ensure] != :absent and not orig_state.include?(resource)
    unless orig_state.include?(resource)
      puts resource.to_manifest
    end
  end
end
