#!/bin/bash

cd "$(dirname "$0")"

die() {
    echo "$1" >&2
    exit 1
}

# === Defaults ===

default_url="master@puppet:~/puppet/modules"

# create variables based on defaults
for opt in url; do
    varname="default_$opt"
    declare $opt=${!varname}
done

# read config from file
conf_file="$(dirname $0)/moduleman.conf"
[[ -r "$conf_file" ]] && source "$conf_file"

# Can't use https://code.google.com/p/shflags/ here. Missing getopt under Windows.
# === Parse Options ===
while [[ -n "$1" ]]; do
case "$1" in
-h|--help)
    echo "$(basename $0) [options] action modulename"
    echo
    echo "action must be 'init' or 'update'"
    echo "modulename must obey puppet's naming rules (match ^[a-z][a-z0-9_]*\$)"
    echo
    echo "Options:"
    echo "--url url         Url to module: user@host:module_dest_dir. default: $default_url"
    exit
    ;;
--url)
    declare ${1:2}="$2" # cut "--" from $1, use it as variable name and assign it a value from $2
    shift 2
    ;;
-*)
    die "error: no such option \"$1\". -h for help"
    ;;
*)
    break 2
esac
done

[[ $# -eq 2 ]] \
    || die "expected 2 positional arguments (see --help for more info)"

action="$1"
modulename="$2"


# === Checks ===

[[ "$action" = "init" || "$action" = "update" ]] \
    || die "error: wrong action \"$action\""

# http://docs.puppetlabs.com/puppet/3/reference/modules_fundamentals.html#allowed-module-names
# can't use =~ operator, because bash3 compability is needed
echo "$modulename" | grep "^[a-z][a-z0-9_]*$" >/dev/null \
    || die "error: invalid modulename \"$modulename\""


# === Main ===

resources_dir="$PWD/resources"

# mktemp may be unavailable on win32
# http://www.linuxsecurity.com/content/view/115462/151/
tmpdir="${TMPDIR-/tmp}/moduleman.$RANDOM.$RANDOM.$RANDOM.$$"
mkdir -m 700 $tmpdir \
    || die "Could not create temporary directory in \"${TMPDIR-/tmp}\""
trap "cd; rm -fr \"${tmpdir}\"" EXIT

echo "info: cloning module repo"
cd "$tmpdir"
git clone --quiet "${url}/${modulename}" . \
    || die "error: could not clone git repository"
git config user.name "module client side script"
git config user.email "$USER@$(hostname)"
git config push.default current


if [[ "$action" = "init" ]]; then
    [[ ! -d .init ]] \
        || die "error: module already initialized"
    mkdir .init
else # update
    [[ -d .init ]] \
        || die "error: module not initialized"
    echo "class $modulename {" >manifests/init.pp
fi

echo "info: running resource scripts"
for resource in $resources_dir/*; do if [[ -x "$resource" ]]; then
    resource_name="$(basename $resource)"
    echo "info: resource $resource_name"
    
    if [[ "$action" = "init" ]]; then
        "$resource" init </dev/null >.init/$resource_name
    else # update
        "$resource" update <.init/$resource_name >>manifests/init.pp
    fi || die "error: $resource_name failed"
fi done

if [[ "$action" = "init" ]]; then
    git add .init
else # update
    echo "}" >>manifests/init.pp
fi


echo "info: pushing new configuration"
git commit --quiet -a -m "client side $action"
git push --quiet

echo "info: operation successfully finished"
