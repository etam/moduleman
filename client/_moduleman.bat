@echo off

rem This file is a compilation of puppet's lauch scripts and git-bash

cd %~dp0

call config.bat

rem puppet
call %PUPPET_PATH%\environment.bat %0 %*

rem Git/bash
if "%PROCESSOR_ARCHITECTURE%" == "AMD64" set COMSPEC=%WINDIR%\SysWOW64\cmd.exe
set MSYSTEM=MSYS

rem moduleman
set /P MODULENAME=modulename? || goto:eof
call %COMSPEC% /c %GIT_PATH%\bin\bash.exe --login moduleman.sh %1 %MODULENAME%
pause
